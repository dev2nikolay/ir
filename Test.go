package main

import (
	"fmt"
	"os"
	"github.com/emirpasic/gods/lists/arraylist"
	"github.com/emirpasic/gods/sets/hashset"
	"strings"
	"encoding/gob"
	"unicode"
	"io"
	"bufio"
	"bytes"
	"regexp"
	"github.com/emirpasic/gods/maps/hashmap"
)

var vocabulary = hashset.New()
var count = int(0)
var index = hashmap.New()

func main() {
	list := arraylist.New()
	list.Add("1.txt", "2.txt", "3.txt", "4.txt", "5.txt", "6.txt", "7.txt",
		"8.txt", "9.txt", "10.txt", "11.txt", "12.txt", "13.txt")

	list.Each(ReadFile)

	fmt.Println(vocabulary.Size())
	fmt.Println(count)


	//SaveSetAsJsonFile(vocabulary, "vocabulary.json")
	//SaveSetAsBinaryFile(vocabulary, "vocabulary.bin")
	//SaveSetAsCsvFile(vocabulary, "vocabulary.csv")

}
func SaveSetAsJsonFile(set *hashset.Set, s string) {
	f, err := os.Create(s)
	check(err)
	bt, _ := vocabulary.ToJSON()
	n3, err := f.Write(bt)
	check(err)
	fmt.Printf("wrote %d bytes\n", n3)
	f.Close()
}
func GenList(list []interface{}) func() string {
	i := 0
	n := len(list)
	return func() string {
		if i < n {
			i++
		}
		return list[i].(string)
	}
}

func SaveSetAsBinaryFile(set *hashset.Set, s string) {
	f, err := os.Create(s)
	check(err)
	//buf := new(bytes.Buffer)

	encoder := gob.NewEncoder(f)
	encoder.Encode(set.Values())

	//for _, v := range set.Values() {
	//	binary.Write(buf, binary.BigEndian, v.(string))
	//}
	//f.Write(buf.Bytes())

	//fmt.Printf("%x", buf.Bytes())
	f.Close()

}
func SaveSetAsCsvFile(set *hashset.Set, s string) {
	f, err := os.Create(s)
	check(err)
	var arr []string
	for _, v := range set.Values() {
		arr = append(arr, v.(string))
	}
	n3, err := f.WriteString(strings.Join(arr, ","))
	check(err)
	fmt.Printf("wrote %d bytes\n", n3)
	f.Close()

}
func check(e error) {
	if e != nil {
		panic(e)
	}
}
func ReadFile(_ int, filename interface{}) {

	f, _ := os.Open(filename.(string))
	scanner := bufio.NewScanner(f)
	//s := NewScanner(f)
	//tok, lit := s.Scan()
	//stmt, err := NewParser(f).Parse()
	//fmt.Println(tok,lit)
	//fmt.Println(stmt.Words,stmt.Count, err)

	scanner.Split(bufio.ScanWords)
	for scanner.Scan() {
		reg, err := regexp.Compile("[^a-zA-Z0-9]+")
		check(err)
		processedString := reg.ReplaceAllString(scanner.Text(), "")
		vocabulary.Add(processedString)
		index.Put(processedString,filename)
		count++
	}
	f.Close()

}

/////////////////////////////////////
type Dictionary struct {
	Words []string
	Count int
}

// Parser represents a parser.
type Parser struct {
	s *Scanner
	buf struct {
		tok Token  // last read token
		lit string // last read literal
		n   int    // buffer size (max=1)
	}
}

// NewParser returns a new instance of Parser.
func NewParser(r io.Reader) *Parser {
	return &Parser{s: NewScanner(r)}
}

// Parse parses a Wordstring statement.
func (p *Parser) Parse() (*Dictionary, error) {
	stmt := &Dictionary{}

	// Next we should loop over all our comma-delimited words.
	for {
		// Read a word.
		tok, lit := p.scanIgnoreWhitespace()
		if tok != IDENT {
			continue
			//return nil, fmt.Errorf("found %q, expected word", lit)
		}
		stmt.Words = append(stmt.Words, lit)

		//// If the next token is not a comma then break the loop.
		//if tok, _ := p.scanIgnoreWhitespace(); tok != COMMA && tok != ASTERISK && tok != WS {
		//	p.unscan()
		//	break
		//}
	}

	// Finally we should read the table name.
	//tok, lit := p.scanIgnoreWhitespace()
	//if tok != IDENT {
	//	return nil, fmt.Errorf("found %q, expected table name", lit)
	//}
	stmt.Count++

	// Return the successfully parsed statement.
	return stmt, nil
}

// scan returns the next token from the underlying scanner.
// If a token has been unscanned then read that instead.
func (p *Parser) scan() (tok Token, lit string) {
	// If we have a token on the buffer, then return it.
	if p.buf.n != 0 {
		p.buf.n = 0
		return p.buf.tok, p.buf.lit
	}

	// Otherwise read the next token from the scanner.
	tok, lit = p.s.Scan()

	// Save it to the buffer in case we unscan later.
	p.buf.tok, p.buf.lit = tok, lit

	return
}

// scanIgnoreWhitespace scans the next non-whitespace token.
func (p *Parser) scanIgnoreWhitespace() (tok Token, lit string) {
	tok, lit = p.scan()
	if tok == WS {
		tok, lit = p.scan()
	}
	return
}

// unscan pushes the previously read token back onto the buffer.
func (p *Parser) unscan() { p.buf.n = 1 }

// Token represents a lexical token.
type Token int

const (
	// Special tokens
	ILLEGAL Token = iota
	EOF
	WS

	// Literals
	IDENT  // main

	// Misc characters
	ASTERISK  // *
	COMMA     // ,
)

// Scanner represents a lexical scanner.
type Scanner struct {
	r *bufio.Reader
}

// NewScanner returns a new instance of Scanner.
func NewScanner(r io.Reader) *Scanner {
	return &Scanner{r: bufio.NewReader(r)}
}

// Scan returns the next token and literal value.
func (s *Scanner) Scan() (tok Token, lit string) {
	// Read the next rune.
	ch := s.read()

	// If we see whitespace then consume all contiguous whitespace.
	// If we see a letter then consume as an ident or reserved word.
	// If we see a digit then consume as a number.
	if isWhitespace(ch) {
		s.unread()
		return s.scanWhitespace()
	} else if isLetter(ch) {
		s.unread()
		return s.scanIdent()
	}

	// Otherwise read the individual character.
	switch ch {
	case eof:
		return EOF, ""
	case '*':
		return ASTERISK, string(ch)
	case ',':
		return COMMA, string(ch)
	}

	return ILLEGAL, string(ch)
}

// scanWhitespace consumes the current rune and all contiguous whitespace.
func (s *Scanner) scanWhitespace() (tok Token, lit string) {
	// Create a buffer and read the current character into it.
	var buf bytes.Buffer
	buf.WriteRune(s.read())

	// Read every subsequent whitespace character into the buffer.
	// Non-whitespace characters and EOF will cause the loop to exit.
	for {
		if ch := s.read(); ch == eof {
			break
		} else if !isWhitespace(ch) {
			s.unread()
			break
		} else {
			buf.WriteRune(ch)
		}
	}

	return WS, buf.String()
}

// scanIdent consumes the current rune and all contiguous ident runes.
func (s *Scanner) scanIdent() (tok Token, lit string) {
	// Create a buffer and read the current character into it.
	var buf bytes.Buffer
	buf.WriteRune(s.read())

	// Read every subsequent ident character into the buffer.
	// Non-ident characters and EOF will cause the loop to exit.
	for {
		if ch := s.read(); ch == eof {
			break
		} else if !isLetter(ch) && !isDigit(ch) && ch != '_' {
			s.unread()
			break
		} else {
			_, _ = buf.WriteRune(ch)
		}
	}

	// Otherwise return as a regular identifier.
	return IDENT, buf.String()
}

// read reads the next rune from the buffered reader.
// Returns the rune(0) if an error occurs (or io.EOF is returned).
func (s *Scanner) read() rune {
	ch, _, err := s.r.ReadRune()
	if err != nil {
		return eof
	}
	return ch
}

// unread places the previously read rune back on the reader.
func (s *Scanner) unread() { _ = s.r.UnreadRune() }

// isWhitespace returns true if the rune is a space, tab, or newline.
func isWhitespace(ch rune) bool { return ch == ' ' || ch == '\t' || ch == '\n' || ch == '\r' }

// isLetter returns true if the rune is a letter.
func isLetter(ch rune) bool { return (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || unicode.Is(unicode.Cyrillic, ch) }

// isDigit returns true if the rune is a digit.
func isDigit(ch rune) bool { return (ch >= '0' && ch <= '9') }

// eof represents a marker rune for the end of the reader.
var eof = rune(0)
