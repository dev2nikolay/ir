package bsbi.io.impl;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bsbi.io.TermListWriterIntf;
import bsbi.models.impl.TermPostingsList;

public class TextFileTermListWriter implements TermListWriterIntf {
    private PrintWriter writer;
    private File file;
    private HashMap<Integer, TermPostingsList> termPostingsLists;

    public TextFileTermListWriter(
            HashMap<Integer, TermPostingsList> termPostingsLists,
            int blockNumber) {
        this.termPostingsLists = termPostingsLists;
        String path = "blocks" + File.separator+blockNumber + ".txt";
        this.file = new File(path);
        try {
            initFile(this.file);
            writer = new PrintWriter(new BufferedWriter(new FileWriter(this.file)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(blockNumber + "  " + termPostingsLists.toString());
        // TODO Auto-generated constructor stub
    }

    private void initFile(File file) throws IOException {

        File parentDirectory = new File(file.getParent());

        if (!file.exists()) {
            file.createNewFile();
        } else if (file.exists()) {
            file.delete();
            file.createNewFile();
        } else if (!parentDirectory.exists()) { // No parent directory
            parentDirectory.mkdir();
            file.createNewFile();
        }

    }

    @Override
    public void write() {
        for (Map.Entry<Integer, TermPostingsList> entry : this.termPostingsLists.entrySet()) {
            TermPostingsList termPostingsList = entry.getValue();

            ArrayList<Integer> postingsList = new ArrayList<Integer>(termPostingsList.getPostingsList());
            String line = "";
            line += termPostingsList.getTermId() + ";";
//            line += termPostingsList.getTerm() + ","+termPostingsList.getTermId()+";";
            for (int docId : postingsList) {
                line += docId + ",";
            }
            //System.out.println(line);
            writer.println(line);
        }
        writer.close();
        // TODO Auto-generated method stub

    }

}
